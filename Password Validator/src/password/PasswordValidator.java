package password;

/*
 * @author Hung Han Chen, 991531300
 * This class validates passwords and it will be developing TDD
 * */

public class PasswordValidator {

	private static int MIN_LENGTH = 8;
	private static int MIN_DIGIT_COUNT = 2;

	public static boolean isValidLength(String password) {
		if (password != null) {
			return password.trim().length() >= MIN_LENGTH;
		}

		return false;
	}

	public static boolean hasEnEnoughDigits(String password) {
		if (password == null) {
			return false;
		}
		int digitCount = 0;
		char c;
		for (int i = 0; i < password.length(); i++) {
			c = password.charAt(i);
			if (Character.isDigit(c)) {
				++digitCount;
			}
		}

		return digitCount >= MIN_DIGIT_COUNT;
	}
}
